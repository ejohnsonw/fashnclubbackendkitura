/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageCollectionRouter.swift
Description:  ImageCollection Handlers for REST endpoints 
Project:      Fashn
Template: /Kitura/server/EntityRouterClass.swift.vm
 */


class ImageCollectionRouter  { 
	init(){
	 Router.sharedInstance.get("/api/imageCollection"){ request, response, next in
	    do{
	        let imageCollections : [ImageCollection]  = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.list()
	        print (NSJSONSerialization.isValidJSONObject (imageCollections ))
	  	
	        let json = try ImageCollection.encodeList(imageCollections );
	        try response.outputJson(json)
	  	}catch{
	  	  response.setStatus (500, message: "Could not list ImageCollection data")
	  	}
	    //response.appendBodyString("Index handler: You accessed path \(request.requestURI())")
	    next()
	  }
	
	
	  Router.sharedInstance.post("/api/imageCollection"){ request, response, next in
	     let imageCollection = ImageCollection() 
	     do {
	    	try imageCollection.decode(request.postBodyString);
	    	let result = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.insert(imageCollection)
	    	let json = try imageCollection.encode()
	    	try response.outputJson(json)
	    }catch{
	        response.appendBodyString("Error accessing data:  \(error)")
	    }
	    next()
	 }
	
	
	 Router.sharedInstance.get("/api/imageCollection/:id"){ request, response, next in
	   let id = Int(request.params["id"]!)
	    do{
	        let imageCollection : ImageCollection  = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.retrieve(id!)!
	        let json = try imageCollection.encode()
	        try response.outputJson(json)
	    }catch{
	        response.setStatus (500, message: "Could not retrieve ImageCollection \(id) data")
	    }
	    next()
	 }
	
	 Router.sharedInstance.put("/api/imageCollection"){ request, response, next in
	    do {
	     	let imageCollection = ImageCollection() 
	    	try imageCollection.decode(request.postBodyString);
	    	let result = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.update(imageCollection)
	    	let json = try imageCollection.encode()
	    	try response.outputJson(json)
	    }catch{
	        response.appendBodyString("Error accessing data:  \(error)")
	    }
	    next()
	 }
	
	
	 Router.sharedInstance.delete("/api/imageCollection/:id"){ request, response, next in
	    let id = Int(request.params["id"]!)
	    do{
	        let result = try PersistenceManagerMySQL.sharedInstance.imageCollectionRepository.delete(id!)
	        //let json = try imageCollection.encode()
	        try response.outputJson("{\"id\":\(id),\"message\":\"deleted\"}")
	    }catch{
	        response.setStatus (500, message: "Could not delete ImageCollection \(id) data")
	    }
	    next()
	 }
  }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 27.16 minutes to type the 2716+ characters in this file.
 */


