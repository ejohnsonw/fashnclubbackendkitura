/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     AssetRouter.swift
Description:  Asset Handlers for REST endpoints 
Project:      Fashn
Template: /Kitura/server/EntityRouterClass.swift.vm
 */


class AssetRouter  { 
	init(){
	 Router.sharedInstance.get("/api/asset"){ request, response, next in
	    do{
	        let assets : [Asset]  = try PersistenceManagerMySQL.sharedInstance.assetRepository.list()
	        print (NSJSONSerialization.isValidJSONObject (assets ))
	  	
	        let json = try Asset.encodeList(assets );
	        try response.outputJson(json)
	  	}catch{
	  	  response.setStatus (500, message: "Could not list Asset data")
	  	}
	    //response.appendBodyString("Index handler: You accessed path \(request.requestURI())")
	    next()
	  }
	
	
	  Router.sharedInstance.post("/api/asset"){ request, response, next in
	     let asset = Asset() 
	     do {
	    	try asset.decode(request.postBodyString);
	    	let result = try PersistenceManagerMySQL.sharedInstance.assetRepository.insert(asset)
	    	let json = try asset.encode()
	    	try response.outputJson(json)
	    }catch{
	        response.appendBodyString("Error accessing data:  \(error)")
	    }
	    next()
	 }
	
	
	 Router.sharedInstance.get("/api/asset/:id"){ request, response, next in
	   let id = Int(request.params["id"]!)
	    do{
	        let asset : Asset  = try PersistenceManagerMySQL.sharedInstance.assetRepository.retrieve(id!)!
	        let json = try asset.encode()
	        try response.outputJson(json)
	    }catch{
	        response.setStatus (500, message: "Could not retrieve Asset \(id) data")
	    }
	    next()
	 }
	
	 Router.sharedInstance.put("/api/asset"){ request, response, next in
	    do {
	     	let asset = Asset() 
	    	try asset.decode(request.postBodyString);
	    	let result = try PersistenceManagerMySQL.sharedInstance.assetRepository.update(asset)
	    	let json = try asset.encode()
	    	try response.outputJson(json)
	    }catch{
	        response.appendBodyString("Error accessing data:  \(error)")
	    }
	    next()
	 }
	
	
	 Router.sharedInstance.delete("/api/asset/:id"){ request, response, next in
	    let id = Int(request.params["id"]!)
	    do{
	        let result = try PersistenceManagerMySQL.sharedInstance.assetRepository.delete(id!)
	        //let json = try asset.encode()
	        try response.outputJson("{\"id\":\(id),\"message\":\"deleted\"}")
	    }catch{
	        response.setStatus (500, message: "Could not delete Asset \(id) data")
	    }
	    next()
	 }
  }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 23.86 minutes to type the 2386+ characters in this file.
 */


