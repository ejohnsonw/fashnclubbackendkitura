/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     CompositionRouter.swift
Description:  Composition Handlers for REST endpoints 
Project:      Fashn
Template: /Kitura/server/EntityRouterClass.swift.vm
 */


class CompositionRouter  { 
	init(){
	 Router.sharedInstance.get("/api/composition"){ request, response, next in
	    do{
	        let compositions : [Composition]  = try PersistenceManagerMySQL.sharedInstance.compositionRepository.list()
	        print (NSJSONSerialization.isValidJSONObject (compositions ))
	  	
	        let json = try Composition.encodeList(compositions );
	        try response.outputJson(json)
	  	}catch{
	  	  response.setStatus (500, message: "Could not list Composition data")
	  	}
	    //response.appendBodyString("Index handler: You accessed path \(request.requestURI())")
	    next()
	  }
	
	
	  Router.sharedInstance.post("/api/composition"){ request, response, next in
	     let composition = Composition() 
	     do {
	    	try composition.decode(request.postBodyString);
	    	let result = try PersistenceManagerMySQL.sharedInstance.compositionRepository.insert(composition)
	    	let json = try composition.encode()
	    	try response.outputJson(json)
	    }catch{
	        response.appendBodyString("Error accessing data:  \(error)")
	    }
	    next()
	 }
	
	
	 Router.sharedInstance.get("/api/composition/:id"){ request, response, next in
	   let id = Int(request.params["id"]!)
	    do{
	        let composition : Composition  = try PersistenceManagerMySQL.sharedInstance.compositionRepository.retrieve(id!)!
	        let json = try composition.encode()
	        try response.outputJson(json)
	    }catch{
	        response.setStatus (500, message: "Could not retrieve Composition \(id) data")
	    }
	    next()
	 }
	
	 Router.sharedInstance.put("/api/composition"){ request, response, next in
	    do {
	     	let composition = Composition() 
	    	try composition.decode(request.postBodyString);
	    	let result = try PersistenceManagerMySQL.sharedInstance.compositionRepository.update(composition)
	    	let json = try composition.encode()
	    	try response.outputJson(json)
	    }catch{
	        response.appendBodyString("Error accessing data:  \(error)")
	    }
	    next()
	 }
	
	
	 Router.sharedInstance.delete("/api/composition/:id"){ request, response, next in
	    let id = Int(request.params["id"]!)
	    do{
	        let result = try PersistenceManagerMySQL.sharedInstance.compositionRepository.delete(id!)
	        //let json = try composition.encode()
	        try response.outputJson("{\"id\":\(id),\"message\":\"deleted\"}")
	    }catch{
	        response.setStatus (500, message: "Could not delete Composition \(id) data")
	    }
	    next()
	 }
  }
}

/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 25.84 minutes to type the 2584+ characters in this file.
 */


